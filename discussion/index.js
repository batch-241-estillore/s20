//Repetition Control Structures

//While loop
/*
	syntax:
		while(expression/condition){
			statements
		}
*/

for(let count=0; count<=20; count++){
	if(count%2==0){
		continue;
	}
	console.log("Continue and break "+count);
	if(count == 5){
		break;
	}
}


let myString = 'john';
console.log(myString.length);

for(let x =0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "Gab";

for(let i =0; i< myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		) {
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}


//Continue and break

/*
	The "continue" statement allows the code to go to the next iteration of a loop without finishing the execution of a statements in a code block.
	The "break" statement is used to terminate the loop once a match has been found.
*/


for (let i = 0; i <= 20; i++){

	if(i%2 === 0){
		continue;
	}

	console.log("Continue and break "+i);
	
	if(i>10){
		break;
	}
}


let name = "johngab";

for(let i=0; i<name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase()==='a'){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i]=='d'){
		break;
	}
}


